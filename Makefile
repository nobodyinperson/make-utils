# make-utils
MAKE_UTILS_PATH = . # relative path to make-utils repo folder
MAKE_UTILS_INCLUDES = $(wildcard $(realpath $(MAKE_UTILS_PATH))/*.mk) # find function files

TEST_MAKEFILE = TestMakefile

# files
DOCS_DIR = docs
DOCS_BUILD_DIR = $(DOCS_DIR)/build
DOC_RST = $(DOCS_BUILD_DIR)/index.rst
APIDOC_RST = $(DOCS_DIR)/api.rst
DOC_HTML = $(addprefix $(DOCS_BUILD_DIR)/,$(notdir $(DOC_RST:.rst=.html)))
APIDOC_INTRO_FILES = $(wildcard $(DOCS_DIR)/*intro.rst)
APIDOC_OUTRO_FILES = $(wildcard $(DOCS_DIR)/*outro.rst)

TEMP_DIRS += $(DOCS_BUILD_DIR)

vpath %.rst $(DOCS_DIR)

###############
### targets ###
###############
.PHONY: all
all: docs

.PHONY: docs
docs: $(DOC_HTML)

$(APIDOC_RST): $(MAKE_UTILS_INCLUDES)
	cat $^ | perl -ne 'print if (s/^# //g)' > $@

$(DOC_RST): $(APIDOC_INTRO_FILES) $(APIDOC_RST) $(APIDOC_OUTRO_FILES) \
	| $(dir $(DOC_RST))
	cat $^ > $@

%.html: %.rst
	pandoc --standalone -t html -o $@ -i $<

$(patsubst %,%/,$(patsubst %/,%,$(TEMP_DIRS))): % :
	mkdir -p $@

.PHONY: clean
clean:
	rm -rf $(APIDOC_RST) $(DOC_RST) $(DOC_HTML)

